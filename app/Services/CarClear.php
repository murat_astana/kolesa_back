<?php
namespace App\Services;
use App\Models\Car;
use App\Models\SearchUrl;

class CarClear {

    static function parse(){
        $date = new \DateTime();
        $date->modify('-7 days');
        $date = $date->format('Y-m-d');

        Car::where('post_date', '<', $date)->delete();
    }
}