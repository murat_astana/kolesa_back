<?php
namespace App\Services;
use App\Models\Car;
use App\Models\SearchUrl;
use Goutte\Client;

class CarParser {
    private $client;
    private $car_links = [];
    CONST SITE_URL = 'https://kolesa.kz';
    private $url_user;


    function __construct(){
        $this->client = new Client();
        $this->url_user = SearchUrl::pluck('user_id', 'id')->toArray();
    }

    static function parse(){
        $el = new CarParser();
        $el->start();
    }

    function start(){
        $this->calcCarLink();
        $this->parseCarLink();

    }

    private function parseCarLink(){
        if (count($this->car_links) == 0)
            return;

        foreach ($this->car_links as $url_id => $ar){
            $ar_car_by_price = Car::where('url_id', $url_id)->pluck('price', 'url')->toArray();
            foreach ($ar as $url => $price){
                //  цена машины не изменены
                if (isset($ar_car_by_price[$url]) && $ar_car_by_price[$url] == $price){
                    continue;
                }

                // цена машины  изменена
                if (isset($ar_car_by_price[$url]) && $ar_car_by_price[$url] != $price) {
                    $this->updateCar($url, $url_id);
                    continue;
                }

                $this->createCar($url, $url_id);
            }



        }
    }

    private function updateCar($url, $url_id){
        $data = $this->getCarData($url);
        if (!$data) {
            Car::where('url_id', $url_id)->where('url', $url)->delete();
            return;
        }

        Car::where('url_id', $url_id)->where('url', $url)->update($data);
    }

    private function createCar($url, $url_id){
        $data = $this->getCarData($url);
        if (!$data)
            return;

        $data['url_id'] = $url_id;
        $data['user_id'] = $this->url_user[$url_id];
        Car::create($data);
    }

    private function getCarData($url){
        try  {
            $crawler = $this->client->request('GET', $url);
            $data = [];
            $data['url'] = $url;
            // name
            $data['name'] = $crawler->filter('.offer__header .offer__title')->text();

            //price
            if ($crawler->filter('.offer__sidebar-header .offer__price')->count())
                $data['price'] = $this->clearSum($crawler->filter('.offer__sidebar-header .offer__price')->text());

            if ($crawler->filter('.kolesa-score-average .kolesa-score-price')->count())
                $data['price_avg'] = $this->clearSum($crawler->filter('.kolesa-score-average .kolesa-score-price')->text());

            if ($crawler->filter('.kolesa-score  .kolesa-score-label')->count()){
                $percent = $crawler->filter('.kolesa-score  .kolesa-score-label')->text();
                if (strpos($percent, 'дешевле') === false)
                    $data['price_percent'] = $this->clearSum($percent);
                else
                    $data['price_percent'] = - $this->clearSum($percent);
            }

            if ($crawler->filter('.offer__content .offer__description')->count())
                $data['note'] = $crawler->filter('.offer__content .offer__description')->text();

            $data['car_year'] = $crawler->filter('.offer__header >.offer__title > .year')->text();


            // parse column param
            $columns = $crawler->filter('.offer__parameters dl');
            for ($i = 0; $i < $columns->count(); $i++){
                $row = $columns->eq($i);
                $key = $row->filter('dt')->text();
                $value = $row->filter('dd')->text();
                if ($key == 'Город')                    $key = 'city';
                else if ($key == 'Кузов')                    $key = 'car_body';
                else if ($key == 'Объем двигателя, л')       $key = 'engine_volume';
                else if ($key == 'Пробег')                   $key = 'engine_mile';
                else if ($key == 'Коробка передач')          $key = 'transmission';
                else if ($key == 'Растаможен в Казахстане')  $key = 'from_kz';
                else continue;

                if ($key == 'from_kz' && $value == 'Да')
                    $value = true;
                elseif ($key == 'from_kz' && $value == 'Нет')
                    $value = false;

                if ($key == 'engine_mile')
                    $value = $this->clearSum($value);

                if ($key == 'engine_volume') {
                    $value = $this->clearSum($value);
                }

                $data[$key] = $value;
            }


        }
        catch (\Exception $e){
            return false;
        }


        return $data;
    }

    private function clearSum($str) {
        return preg_replace("/[^.0-9]/", '', $str);
    }

    private function calcCarLink(){
        $urls = SearchUrl::all();
        foreach ($urls as $u){
            for ($page = 1; $page <= 100; $page ++){
                print_r($page); echo "\n";
                $crawler = $this->client->request('GET', $u->url . '?page=' . $page);
                $car_links = $crawler->filter('.vw-item.a-elem');
                if ($car_links->count() == 0)
                    break;

                if (!isset($this->car_links[$u->id]))
                    $this->car_links[$u->id] = [];

                for ($i = 0; $i < $car_links->count(); $i++){
                    $node = $car_links->eq($i);
                    // calc link
                    $link = $node->filter('.a-el-info-title > .list-link.ddl_product_link ')->attr('href');
                    $link = static::SITE_URL.$link;
                    // calc sum
                    $sum = $node->filter('.a-info-top > .price')->text();
                    $sum = preg_replace("/[^0-9]/", '', $sum);

                    $this->car_links[$u->id][$link] = $sum;
                }

            }

            echo "_______ count ---- "; echo count($this->car_links[$u->id]);  echo "\n";
        }

    }




}