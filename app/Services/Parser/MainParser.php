<?php
namespace App\Services\Parser;

use App\Events\ParseSearchUrlEvent;
use App\Models\SearchUrl;

class  MainParser {

    static function parse(){
        $urls = SearchUrl::all();
        foreach ($urls as $u){
            event(new ParseSearchUrlEvent($u));
        }
    }

}