<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class NewCarNotification extends Notification
{
    private $el;

    public function __construct($el){
        $this->el = $el;
    }

    public function via($notifiable) {
        return [TelegramChannel::class];
    }

    public function toTelegram($notifiable)  {
        $url = $this->el->url;

        return TelegramMessage::create()
            ->to($notifiable->telegram_user_id)
            ->content($this->getText())
            ->button('Перейти по ссылке', $url);
    }

    private function getText(){
        // Заголовок
        $text = "".$this->el->name." \n";
        $text .= 'Цена : '.number_format($this->el->price, 2, ',', ' ')."\n";
        if ($this->el->price_percent) {
            $text .= "Средняя цена : " . number_format($this->el->price_avg, 2, ',', ' ') . "\n";
            $text .= "Процент : " . $this->el->price_percent. "\n";
        }

        $text .= "Растаможено : " . ($this->el->from_kz ? "Да" : 'Нет'). "\n";

        if ($this->el->car_year)
            $text .= "Год : " . $this->el->car_year. "\n";

        if ($this->el->engine_mile)
            $text .= "Пробег : " . $this->el->engine_mile. "\n";

        if ($this->el->transmission)
            $text .= "Коробка : " . $this->el->transmission. "\n";

        return $text;
    }
}
