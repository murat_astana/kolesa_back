<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;
    protected $table = 'cars';
    protected $fillable = ['url_id', 'user_id', 'url', 'name', 'price', 'price_avg', 'price_percent',
                            'city', 'engine_volume', 'engine_type', 'transmission', 'from_kz', 'car_year', 'car_body',
                            'phone', 'note', 'comment', 'is_active', 'post_date', 'engine_mile'];

    function relSearch(){
        return $this->belongsTo(SearchUrl::class, 'url_id');
    }

    function scopeFilter($q, $request){
        if ($request->has('name') && trim($request->name) != '')
            $q->where('name', 'like', '%'.$request->name.'%');

        if ($request->has('city') && trim($request->city) != '')
            $q->where('city', 'like', '%'.$request->city.'%');

        if ($request->has('engine_type') && trim($request->engine_type) != '')
            $q->where('engine_type', 'like', '%'.$request->engine_type.'%');

        if ($request->has('transmission') && trim($request->transmission) != '')
            $q->where('transmission', 'like', '%'.$request->transmission.'%');

        if ($request->has('car_body') && trim($request->car_body) != '')
            $q->where('car_body', 'like', '%'.$request->car_body.'%');

        if ($request->has('note') && trim($request->note) != '')
            $q->where('note', 'like', '%'.$request->note.'%');

        if ($request->has('url_id') && $request->url_id)
            $q->where('url_id', $request->url_id);

        if ($request->has('price_b') && $request->price_b)
            $q->where('price', '>=', $request->price_b);

        if ($request->has('price_e') && $request->price_e)
            $q->where('price', '<=', $request->price_e);

        if ($request->has('price_avg_b') && $request->price_avg_b)
            $q->where('price_avg', '>=', $request->price_avg_b);

        if ($request->has('price_avg_e') && $request->price_avg_e)
            $q->where('price_avg', '<=', $request->price_avg_e);

        if ($request->has('price_percent_b') && $request->price_percent_b)
            $q->where('price_percent', '>=', $request->price_percent_b);

        if ($request->has('price_percent_e') && $request->price_percent_e)
            $q->where('price_percent', '<=', $request->price_percent_e);

        if ($request->has('engine_volume_b') && $request->engine_volume_b)
            $q->where('engine_volume', '>=', $request->engine_volume_b);

        if ($request->has('engine_volume_e') && $request->engine_volume_e)
            $q->where('engine_volume', '<=', $request->engine_volume_e);

        if ($request->has('car_year_b') && $request->car_year_b)
            $q->where('car_year', '>=', $request->car_year_b);

        if ($request->has('car_year_e') && $request->car_year_e)
            $q->where('car_year', '<=', $request->car_year_e);

        if ($request->has('from_kz') && $request->from_kz == 'Y')
            $q->where('from_kz', 1);

        if ($request->has('from_kz') && $request->from_kz == 'N')
            $q->where('from_kz', 0);

        return $q;
    }

    function scopeSort($q, $request){
        $q->orderBy('price_percent', 'ASC');

        return $q;
    }
}
