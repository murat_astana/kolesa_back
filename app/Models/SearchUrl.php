<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SearchUrl extends Model
{
    use HasFactory;
    protected $table = 'search_url';
    protected $fillable = ['user_id', 'name', 'url', 'is_active'];

    function scopeFilter($q, $request){
        return $q;
    }
}
