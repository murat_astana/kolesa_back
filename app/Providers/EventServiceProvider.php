<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        \App\Events\ParseSearchUrlEvent::class => [
            \App\Listeners\ParseSearchUrlListner::class
        ],
        \App\Events\NewCarEvent::class => [
            \App\Listeners\NewCarListner::class
        ],
        \App\Events\UpdateCarEvent::class => [
            \App\Listeners\UpdateCarListner::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
        parent::boot();
    }
}
