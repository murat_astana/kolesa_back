<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CarResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'url_id' => $this->url_id,
            'url_name' => $this->relSearch->name,
            'url' => $this->url,
            'name' => $this->name,
            'price' => $this->price,
            'price_avg' => $this->price_avg,
            'price_percent' => $this->price_percent,
            'city' => $this->city,
            'engine_volume' => $this->engine_volume,
            'engine_type' => $this->engine_type,
            'transmission' => $this->transmission,
            'phone' => $this->phone,
            'note' => $this->note,
            'comment' => $this->comment,
            'is_active' => (boolean)$this->is_active,
            'post_date' => $this->post_date,
            'from_kz' => $this->from_kz,
            'car_year' => $this->car_year,
            'car_body' => $this->car_body,
            'engine_mile' => $this->engine_mile
        ];
    }
}
