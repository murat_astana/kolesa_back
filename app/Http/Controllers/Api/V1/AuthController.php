<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    function login (Request $request){
        if (!Auth::attempt(['email' => $request->input('login'), 'password' => $request->input('password'), 'is_active' => 1]) )
            return $this->false('Wrong credentials');

        $user = Auth::user();

        return [
            'data' => new UserResource($user),
            'token' =>  $user->createToken('main')->plainTextToken
        ];
    }

    function logout(Request $request){
        $request->user()->tokens()->delete();

        return true;
    }

    function getData(Request $request){
        return new UserResource($request->user());
    }

    function update(Request $request){
        $user = $request->user();
        $user->save();

        return $user;
    }
}
