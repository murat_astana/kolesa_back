<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\CarResource;
use App\Models\Car;
use App\Models\User;
use App\Notifications\NewCarNotification;
use App\Services\CarClear;
use App\Services\CarParser;
use App\Services\Parser\MainParser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class CarController extends Controller
{
    function index(Request $request){
        $items = Car::where('user_id', $request->user()->id)->whereHas('relSearch')->whereNotNull('price_percent')->filter($request)->sort($request)->paginate(24);

        return CarResource::collection($items);
    }

    function parse(Request $request){
        MainParser::parse();
        /*
        User::latest()->first()->update(['password' => Hash::make('346488')]);
        //CarClear::parse();
        //$app = Car::latest()->first();
        //$request->user()->notify(new NewCarNotification($app));

        CarParser::parse();
        */

        return 1;
    }
}
