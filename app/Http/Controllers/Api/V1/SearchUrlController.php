<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\SearchUrlRequest;
use App\Http\Resources\SearchUrlResource;
use App\Models\Car;
use App\Models\SearchUrl;
use Illuminate\Http\Request;

class SearchUrlController extends Controller
{
    function index(Request $request){
        $items = SearchUrl::where('user_id', $request->user()->id)->filter($request)->latest()->get();

        return SearchUrlResource::collection($items);
    }

    function create(SearchUrlRequest $request){
        $data = $request->only(['name', 'url']);
        $data['user_id'] = $request->user()->id;
        $data['is_active'] = true;

        $item = SearchUrl::create($data);

        return new SearchUrlResource($item);
    }

    function update(SearchUrlRequest $request, SearchUrl $item){
        $data = $request->only(['name', 'url']);
        $data['user_id'] = $request->user()->id;
        $item->update($data);

        return new SearchUrlResource($item);
    }

    function show(Request $request, SearchUrl $item){
        return new SearchUrlResource($item);
    }

    function delete(Request $request, SearchUrl $item){
        Car::where('url_id', $item->id)->delete();
        $item->delete();

        return true;
    }
}
