<?php

namespace App\Listeners\Traits;
use Goutte\Client;

trait CarParserTrait {

    function __construct(){
        $this->client = new Client();
    }

    private function clearSum($str) {
        return preg_replace("/[^.0-9]/", '', $str);
    }


    private function getCarData($url){
        try  {
            $crawler = $this->client->request('GET', $url);
            $data = [];
            $data['url'] = $url;
            // name
            $data['name'] = $crawler->filter('.offer__header .offer__title')->text();

            //price
            if ($crawler->filter('.offer__sidebar-header .offer__price')->count())
                $data['price'] = $this->clearSum($crawler->filter('.offer__sidebar-header .offer__price')->text());

            if ($crawler->filter('.kolesa-score-average .kolesa-score-price')->count())
                $data['price_avg'] = $this->clearSum($crawler->filter('.kolesa-score-average .kolesa-score-price')->text());

            if ($crawler->filter('.kolesa-score  .kolesa-score-label')->count()){
                $percent = $crawler->filter('.kolesa-score  .kolesa-score-label')->text();
                if (strpos($percent, 'дешевле') === false)
                    $data['price_percent'] = $this->clearSum($percent);
                else
                    $data['price_percent'] = - $this->clearSum($percent);
            }

            if ($crawler->filter('.offer__content .offer__description')->count())
                $data['note'] = $crawler->filter('.offer__content .offer__description')->text();

            $data['car_year'] = $crawler->filter('.offer__header >.offer__title > .year')->text();


            // parse column param
            $columns = $crawler->filter('.offer__parameters dl');
            for ($i = 0; $i < $columns->count(); $i++){
                $row = $columns->eq($i);
                $key = $row->filter('dt')->text();
                $value = $row->filter('dd')->text();
                if ($key == 'Город')                    $key = 'city';
                else if ($key == 'Кузов')                    $key = 'car_body';
                else if ($key == 'Объем двигателя, л')       $key = 'engine_volume';
                else if ($key == 'Пробег')                   $key = 'engine_mile';
                else if ($key == 'Коробка передач')          $key = 'transmission';
                else if ($key == 'Растаможен в Казахстане')  $key = 'from_kz';
                else continue;

                if ($key == 'from_kz' && $value == 'Да')
                    $value = true;
                elseif ($key == 'from_kz' && $value == 'Нет')
                    $value = false;

                if ($key == 'engine_mile')
                    $value = $this->clearSum($value);

                if ($key == 'engine_volume') {
                    $value = $this->clearSum($value);
                }

                $data[$key] = $value;
            }


        }
        catch (\Exception $e){
            return false;
        }


        return $data;
    }
}