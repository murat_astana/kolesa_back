<?php

namespace App\Listeners;

use App\Events\UpdateCarEvent;
use App\Listeners\Traits\CarParserTrait;
use App\Models\Car;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateCarListner implements ShouldQueue
{
    use CarParserTrait;
    public $delay = 2;

    /**
     * Handle the event.
     *
     * @param  UpdateCarEvent  $event
     * @return void
     */
    public function handle(UpdateCarEvent $event)
    {
        $data = $this->getCarData($event->url);
        if (!$data) {
            Car::where('url_id', $event->url_id)->where('url', $event->url)->delete();
            return;
        }

        Car::where('url_id', $event->url_id)->where('url', $event->url)->update($data);
    }
}
