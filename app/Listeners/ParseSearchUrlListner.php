<?php

namespace App\Listeners;

use App\Events\NewCarEvent;
use App\Events\ParseSearchUrlEvent;
use App\Events\UpdateCarEvent;
use App\Models\Car;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use Goutte\Client;

class ParseSearchUrlListner implements ShouldQueue {
    use InteractsWithQueue;
    CONST SITE_URL = 'https://kolesa.kz';
    private $client;
    public $delay = 20;

    function __construct(){
        $this->client = new Client();
    }

    private function getLink($url, $page){
        if (strpos($url, '?') === false)
            return $url.'?page=' . $page;

        return $url.'&page=' . $page;
    }

    public function handle(ParseSearchUrlEvent $event)
    {
        $url = $event->url;

        $ar_car_link = $this->parsePageLink($url);

        $this->calcCarLink($ar_car_link);
    }

    private function calcCarLink($ar_car_link){
        if (count($ar_car_link) == 0)
            return;

        foreach ($ar_car_link as $url_id => $ar){
            $ar_car_by_price = Car::where('url_id', $url_id)->pluck('price', 'url')->toArray();
            foreach ($ar as $url => $price){
                //  цена машины не изменены
                if (isset($ar_car_by_price[$url]) && $ar_car_by_price[$url] == $price){
                    continue;
                }

                // цена машины  изменена
                if (isset($ar_car_by_price[$url]) && $ar_car_by_price[$url] != $price) {
                    event(new UpdateCarEvent($url, $url_id));
                    continue;
                }

                event(new NewCarEvent($url, $url_id));
            }
        }
    }

    private function parsePageLink($url){
        $ar_car_link = [];

        for ($page = 1; $page <= 2; $page ++){
            $crawler = $this->client->request('GET', $this->getLink($url->url, $page));

            $car_links = $crawler->filter('.vw-item.a-elem');
            if ($car_links->count() == 0)
                break;

            if (!isset($ar_car_link[$url->id]))
                $ar_car_link[$url->id] = [];

            for ($i = 0; $i < $car_links->count(); $i++){
                $node = $car_links->eq($i);
                // calc link
                $link = $node->filter('.a-el-info-title > .list-link.ddl_product_link ')->attr('href');
                $link = static::SITE_URL.$link;
                // calc sum
                $sum = $node->filter('.a-info-top > .price')->text();
                $sum = preg_replace("/[^0-9]/", '', $sum);

                $ar_car_link[$url->id][$link] = $sum;

            }
        }

        return $ar_car_link;
    }
}
