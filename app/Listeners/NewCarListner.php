<?php

namespace App\Listeners;

use App\Events\NewCarEvent;
use App\Listeners\Traits\CarParserTrait;
use App\Models\Car;
use App\Models\User;
use App\Notifications\NewCarNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NewCarListner implements ShouldQueue
{
    use CarParserTrait;
    public $delay = 2;

    public function handle(NewCarEvent $event)
    {
        $data = $this->getCarData($event->url);
        if (!$data)
            return;

        $data['url_id'] = $event->url_id;
        $data['user_id'] = 1;
        $car = Car::create($data);

        if ($car->price_percent && $car->price_avg && $car->price < $car->price_avg && $car->price_percent < -15 && $car->from_kz)
            User::latest()->first()->notify(new NewCarNotification($car));
    }
}
