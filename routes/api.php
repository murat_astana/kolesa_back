<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    Route::post('login', [\App\Http\Controllers\Api\V1\AuthController::class, 'login']);

    Route::group(['middleware' => ['auth:sanctum']], function () {
        Route::group(['prefix' => 'user'], function () {
            Route::get('/', [\App\Http\Controllers\Api\V1\AuthController::class, 'getData']);
            Route::post('/', [\App\Http\Controllers\Api\V1\AuthController::class, 'update']);
        });

        Route::group(['prefix' => 'search-url'], function () {
            Route::get('/', [\App\Http\Controllers\Api\V1\SearchUrlController::class, 'index']);
            Route::post('/', [\App\Http\Controllers\Api\V1\SearchUrlController::class, 'create']);
            Route::get('{item}', [\App\Http\Controllers\Api\V1\SearchUrlController::class, 'show']);
            Route::post('{item}', [\App\Http\Controllers\Api\V1\SearchUrlController::class, 'update']);
            Route::delete('{item}', [\App\Http\Controllers\Api\V1\SearchUrlController::class, 'delete']);
        });


        Route::group(['prefix' => 'car'], function () {
            Route::get('/', [\App\Http\Controllers\Api\V1\CarController::class, 'index']);
            Route::get('parse', [\App\Http\Controllers\Api\V1\CarController::class, 'parse']);

        });

        Route::get('logout', [\App\Http\Controllers\Api\V1\AuthController::class, 'logout']);
    });
});