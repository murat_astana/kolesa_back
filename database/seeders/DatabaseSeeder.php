<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->createAdmin();

    }

    private function createAdmin(){
        if (User::where('email', 'hmurich')->count())
            return;

        User::create([
            'email' => 'hmurich',
            'password' => Hash::make('1491Apolo!'),
            'name' => 'Murat',
            'is_active' => true,
            'is_admin' => true
        ]);
    }
}
