<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Cars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('url_id');
            $table->bigInteger('user_id');
            $table->string('url')->nullable();
            $table->string('name')->nullable();
            $table->integer('car_year')->nullable();
            $table->string('car_body')->nullable();
            $table->string('city')->nullable();
            $table->integer('price')->nullable();
            $table->integer('price_avg')->nullable();
            $table->integer('price_percent')->nullable();
            $table->float('engine_volume')->nullable();
            $table->string('engine_type')->nullable();
            $table->string('engine_mile')->nullable();
            $table->string('transmission')->nullable();
            $table->string('phone')->nullable();
            $table->text('note')->nullable();
            $table->text('comment')->nullable();
            $table->boolean('is_active')->default(false);
            $table->date('post_date')->nullable();
            $table->boolean('from_kz')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
